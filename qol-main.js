function onOpen() { // when the sheet opens

  var ui = SpreadsheetApp.getUi(); // get ui 

  var menu = ui.createMenu("Scripts"); // create menu

  menu.addItem("combine", "combine"); // add menu items

  menu.addItem("shift", "shift");

  menu.addItem("regex shift", "xShift");

  menu.addItem("regex move", "xMove");

  menu.addItem("fill blanks left", "fillBlanksLeft");

  menu.addItem("remove duplicates", "removeDuplicates");

  menu.addItem("repair text", "repairText");

  menu.addItem("repair duplicates", "repairDuplicates");

  menu.addItem("sandbox function", "sandboxFunction");

  menu.addToUi(); // add menu to ui

}



////////////////////////////////////////////////////////////////////////////////////////



function combine() { 
  
  // combine values of two adjacent columns, seperated by a space

  // only works with a selection. selection can only be two adjacent columns wide.

  // if your selection is larger then two columns, it will only combine the first two columns

  var ss = SpreadsheetApp.getActiveSpreadsheet();

  var as = ss.getActiveSheet();

  var ar = as.getActiveRange();

  var vals = ar.getValues();

  // SpreadsheetApp.getUi().alert(vals);

  var x;



  for (x = 0; x < vals.length; x++) {

    vals[x][0] = vals[x][0] + " " + vals[x][1]; //col1 = col1 + col2

    vals[x][1] = ''; //col2 is empty string

  }



  ar.setValues(vals);



  // Logger.log(vals);



}



////////////////////////////////////////////////////////////////////////////////////////



function shift() { 
  
  // shift all cells containing your text parameter to the right by one column

  // if there is data in the adjacent column, it will be replaced with your text parameter

  // only works with a selection. selection can only be two adjacent columns wide.

  // if your selection is larger then two columns, it will only modify the first two columns

  try {

    var ss = SpreadsheetApp.getActiveSpreadsheet();

    var as = ss.getActiveSheet();

    var ar = as.getActiveRange();

    var vals = ar.getValues();

    // SpreadsheetApp.getUi().alert(vals);

    var r;



    for (r = 0; r < vals.length; r++) {

      if (vals[r][0].indexOf("your text parameter here") != -1) {

        //SpreadsheetApp.getUi().alert("found your text parameter");

        var c;

        var cols = [];

        for (c = 0; c < vals[r].length; c++) {

          if (c == 0) {

            cols[c + 1] = vals[r][c];

            cols[c] = "";

          }

          else {

            cols[c + 1] = vals[r][c];

          }

        }

        //SpreadsheetApp.getUi().alert(cols);

        for (c = 0; c < vals[r].length; c++) {

          vals[r][c] = cols[c];

        }

      }

    }

    ar.setValues(vals);

  }

  catch (err) {

    SpreadsheetApp.getUi().alert(err);

  }

}



////////////////////////////////////////////////////////////////////////////////////////



function xShift() { 
  
  // shift all cells containing your regex to the right by one column

  // an input box will apear to take your regex parameter

  // if there is data in the adjacent column, it will be replaced with your text parameter

  // only works with a selection. selection can only be two adjacent columns wide.

  // if your selection is larger then two columns, it will only modify the first two columns

  try {

    var ss = SpreadsheetApp.getActiveSpreadsheet();

    var as = ss.getActiveSheet();

    var ar = as.getActiveRange();

    var vals = ar.getValues();

    var r;

    var ui = SpreadsheetApp.getUi();

    var response = ui.prompt('input regex');

    var regExp = new RegExp(response.getResponseText());



    for (r = 0; r < vals.length; r++) {

      if (regExp.exec(vals[r][0])) {

        var c;

        var cols = [];

        for (c = 0; c < vals[r].length; c++) {

          if (c == 0) {

            cols[c + 1] = vals[r][c];

            cols[c] = "";

          }

          else {

            cols[c + 1] = vals[r][c];

          }

        }

        for (c = 0; c < vals[r].length; c++) {

          vals[r][c] = cols[c];

        }

      }

    }

    ar.setValues(vals);

  }

  catch (err) {

    SpreadsheetApp.getUi().alert(err);

  }

}



////////////////////////////////////////////////////////////////////////////////////////



function xMove() { 
  
  // move all matching cells from your selection to a single column of your choice (regex used for matching)

  // matches will move to the chosen column and maintain their row

  // an input box will apear to take your regex parameter

  // only works with a selection. selection can only be as large as you want

  // selection must start on row 1

  // if there are multiple matches in the same row, they will not be combined, only one match will move to the new column

  try {

    var ss = SpreadsheetApp.getActiveSpreadsheet();

    var as = ss.getActiveSheet();

    var ui = SpreadsheetApp.getUi();



    var ar = as.getActiveRange();

    var vals = ar.getValues();



    var respRegex = ui.prompt('input regex');

    var respCol = ui.prompt('move values to what column?');

    var mr = as.getRange(respCol.getResponseText() + '1' + ':' + respCol.getResponseText() + ar.getHeight().toString());

    var mrVals = mr.getValues();



    var regExp = new RegExp(respRegex.getResponseText());



    ui.alert("moving data to " + mr.getA1Notation());



    for (var r = 0; r < vals.length; r++) {

      var row = vals[r];

      for (var c = 0; c < row.length; c++) {

        var col = row[c];

        if (regExp.exec(col) != null) {

          mrVals[r][0] = col;

          vals[r][c] = "";

        }

      }

    }

    ui.alert(mrVals);

    mr.setValues(mrVals);

    ar.setValues(vals);

  }

  catch (err) {

    ui.alert(err);

  }

}



////////////////////////////////////////////////////////////////////////////////////////



function fillBlanksLeft() { 
  
  // shift all data to the left, filling any available blank cells

  // only works witha selection, selection can be as large as you want

  try {

    var ss = SpreadsheetApp.getActiveSpreadsheet();

    var as = ss.getActiveSheet();

    var ui = SpreadsheetApp.getUi();



    var ar = as.getActiveRange();

    var vals = ar.getValues();



    for (var r = 0; r < vals.length; r++) {

      var row = vals[r];

      for (var c = 0; c < row.length; c++) {

        var cell = row[c];

        if (cell == "" && row[c + 1] == "") {

          continue;

        }

        else if (cell == "" && row[c + 1] != undefined) {

          for (c; c < row.length; c++)

            if (row[c + 1] == undefined) {

              vals[r][c] = "";

            }

            else {

              if (row[c + 1] == "") {

                continue;

              }

              else {

                vals[r][c] = row[c + 1];

                vals[r][c + 1] = "";

              }

            }

        }

      }

    }

    ar.setValues(vals);

  }



  catch (err) {

    ui.alert(err);

  }

}





////////////////////////////////////////////////////////////////////////////////////////



function removeDuplicates() { 
  
  // not currenty working :/ sorry

  try {



    // necessities

    var ui = SpreadsheetApp.getUi();

    var ss = SpreadsheetApp.getActiveSpreadsheet();

    var as = ss.getActiveSheet();





    var sr = as.getDataRange(); // entire sheet range

    var ar = as.getActiveRange(); // active range



    var srv = sr.getValues(); // values of entire sheet

    var arv = ar.getValues(); // values of active range



    var newData = [];







    // basic range iterator. iterates over each cell in each row from left to right and top to bottom

    function iterate(x, arr) {

      for (var r in x) {

        var row = x[r];

        var dup = false;

        for (var c in arr) {

          if (row.join() == arr[c].join()) {

            dup = true;

          }

        }

        if (!dup) {

          arr.push(row);

        }

      }

      as.clearContents();

      as.getRange(1, 1, arr.length, arr[0].length).setValues(arr);

    }







    // main loop

    iterate(srv, newData);





  }

  catch (err) {

    ui.alert(err);

  }

}



////////////////////////////////////////////////////////////////////////////////////////



function repairText() { 
  
  // very case-specific function.. iterates across all values in a single column

  // each row is checked against the previous row for similarity

  // if the similaity check score is over 50%, then make this row equal to the previos row

  try {



    // necessities

    var ui = SpreadsheetApp.getUi();

    var ss = SpreadsheetApp.getActiveSpreadsheet();

    var as = ss.getActiveSheet();





    var sr = as.getDataRange(); // entire sheet range

    var ar = as.getActiveRange(); // active range



    var srv = sr.getValues(); // values of entire sheet

    var arv = ar.getValues(); // values of active range



    function minify(x) {

      var txt = x.trim();

      //ui.alert(txt);

      txt = txt.replace(/ /g, '');

      //ui.alert(txt);

      var arr = txt.split('');

      //ui.alert(arr);

      arr = arr.map(function (a) { return a.toUpperCase() });

      //ui.alert(arr);

      var std = arr.sort();

      //ui.alert(std);

      var reg = new RegExp(/\W{1}/);

      for (var z = 0; z < std.length; z++) {

        if (reg.exec(std[z]) != null) {

          std.splice(z, 1);

        }

      }

      //ui.alert(std);

      return std;

    }





    function compareLength(x, y) { //for comparing arrays

      var xl = x.length;

      var yl = y.length;

      if (xl < yl) {

        for (xl; xl < yl; xl++) {

          x.push("x");

        }

      }

      else if (yl < xl) {

        for (yl; yl < xl; yl++) {

          y.push("x");

        }

      }

    }



    function compareData(x, y) { //for comparing arrays

      var z = [];

      //ui.alert("comparing")

      if (x.toString() == y.toString()) {

        //ui.alert("your strings match");

      }

      else if (x.toString() != y.toString()) {

        var z = x.filter(function (word) {

          return y.indexOf(word) != -1;

        });

      }



      if (z.length / x.length > 0.5) {

        result = "pass";

      }

      else {

        result = "fail";

      }

      //ui.alert(lw);

      return result;

    }



    function checkFirst(r, c, x, y) {

      if (r == 0 && c == 0) {

        x = y;

      }

      return x;

    }





    // basic range iterator. iterates over each cell in each row from left to right and top to bottom

    function iterate(range, values, text) {

      for (var r = 0; r < values.length; r++) {

        var row = values[r];

        for (var c = 0; c < row.length; c++) {

          var cell = row[c];

          var arr = minify(cell);

          //ui.alert(arr);

          var minTxt = minify(text);

          compareLength(minTxt, arr);

          var t = compareData(minTxt, arr);

          //ui.alert(t);

          //ui.alert(text);

          if (t == "pass") {

            values[r][c] = text;

          }

        }

      }

      range.setValues(values);

    }







    // main loop

    var sv = ui.prompt('enter desired string');

    var txt = sv.getResponseText();

    //ui.alert(txt);

    iterate(ar, arv, txt);





  }

  catch (err) {

    ui.alert(err);

  }

}



////////////////////////////////////////////////////////////////////////////////////////



function repairDuplicates() { 
  
  // very case-specific function.. iterates across all values in a single column

  // this is a major improvement/better version of the repairText function

  // each row is checked against the previous row for similarity

  // if the similaity check score is over 50%, then make this row equal to the previos row

  try {



    function ld(s, t) {

      var d = []; //2d matrix



      // Step 1

      var n = s.length;

      var m = t.length;



      if (n == 0) return m;

      if (m == 0) return n;



      //Create an array of arrays in javascript (a descending loop is quicker)

      for (var i = n; i >= 0; i--) d[i] = [];



      // Step 2

      for (var i = n; i >= 0; i--) d[i][0] = i;

      for (var j = m; j >= 0; j--) d[0][j] = j;



      // Step 3

      for (var i = 1; i <= n; i++) {

        var s_i = s.charAt(i - 1);



        // Step 4

        for (var j = 1; j <= m; j++) {



          //Check the jagged ld total so far

          if (i == j && d[i][j] > 4) return n;



          var t_j = t.charAt(j - 1);

          var cost = (s_i == t_j) ? 0 : 1; // Step 5



          //Calculate the minimum

          var mi = d[i - 1][j] + 1;

          var b = d[i][j - 1] + 1;

          var c = d[i - 1][j - 1] + cost;



          if (b < mi) mi = b;

          if (c < mi) mi = c;



          d[i][j] = mi; // Step 6



          //Damerau transposition

          if (i > 1 && j > 1 && s_i == t.charAt(j - 2) && s.charAt(i - 2) == t_j) {

            d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + cost);

          }

        }

      }



      // Step 7

      return d[n][m];

    }





    // necessities

    var ui = SpreadsheetApp.getUi();

    var ss = SpreadsheetApp.getActiveSpreadsheet();

    var as = ss.getActiveSheet();





    var sr = as.getDataRange(); // entire sheet range

    var ar = as.getActiveRange(); // active range



    var srv = sr.getValues(); // values of entire sheet

    var arv = ar.getValues(); // values of active range



    function compareData(x, y) {

      var result;

      if (x.toString() == y.toString()) {

        result = "match";

        //ui.alert("100% " + result);

      }

      else if (x.toString() != y.toString()) {



        var score = ld(x, y);



        //ui.alert(score);



        if (score < 10) {

          result = "pass";

        }

        else {

          result = "fail";

        }

      }

      return result;

    }



    function iterate(range, values, text) {

      for (var r = 0; r < values.length; r++) {

        var row = values[r];

        for (var c = 0; c < row.length; c++) {

          var cell = row[c];

          var t = compareData(text, cell);

          if (t == "pass") {

            values[r][c] = text;

          }

          else if (t == "fail") {

            text = cell;

          }

        }

      }

      range.setValues(values);

    }





    // main loop

    var txt = ar.getValue();

    iterate(ar, arv, txt);

  }

  catch (err) {

    ui.alert(err);

  }

}

////////////////////////////////////////////////////////////////////////////////////////



function sandboxFunction() { 
  
  // a place holder function for testing individual functions

  try {

  }

  catch (err) {

    ui.alert(err);

  }

}



////////////////////////////////////////////////////////////////////////////////////////



function fnTemplate() { 
  
  // a template for an iterative function specifically designed to iterate over your spreadsheet selection

  try {



    // necessities

    var ui = SpreadsheetApp.getUi();

    var ss = SpreadsheetApp.getActiveSpreadsheet();

    var as = ss.getActiveSheet();

    
    


    var asv = as.getDataRange().getValues(); // values of entire sheet
    
    var ar = as.getActiveRange(); // active range

    var arv = ar.getValues(); // values of active range







    // basic range iterator. iterates over each cell in each row from left to right and top to bottom

    function iterate(x) {

      for (var r = 0; r < x.length; r++) {

        var row = x[r];

        for (var c = 0; c < row.length; c++) {

          var cell = row[c];

          ui.alert(cell);

        }

      }

    }







    // main loop

    iterate(arv);





  }

  catch (err) {

    ui.alert(err);

  }

}
